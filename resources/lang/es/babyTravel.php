<?php

return [
    'baby_travel' => 'Viaje del bebé',
    'baby_travel_text' => 'Alquile un equipo de viaje para bebés para menos estrés y menos cargos por equipaje mientras viaja con niños pequeños. El alquiler de equipos para bebés hace que viajar sea más fácil y más cómodo para las familias.',

    'booster_car_seat' => 'Asiento elevador',
    'stroller' => 'Cochecitos',
    'infant_car_seat' => 'Asiento infantil',
    'safety_first_car_seat' => 'Primera silla de seguridad'
];