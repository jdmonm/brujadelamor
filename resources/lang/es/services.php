<?php

return [
    'services' => 'SERVICIOS',
    'services_text' => 'Contamos con el mejor servicio y equipo, somos una empresa con una gran trayectoria y experiencia en el mercado en este tipo de servicios, garantizando la satisfacción de nuestros clientes y haciendo de su experiencia algo inolvidable.', 

    'corporate_bussines' => 'Corporate Business',
    'corporate_bussines_text' => 'Ofrecemos al viajero corporativo de negocios un servicio puerta a puerta que ofrece transporte directo entre dos ubicaciones geográficas, y es más eficiente para viajes desde y hacia los aeropuertos, servicios de chóferes dirigidos, de ciudad a ciudad, diseñados para satisfacer todas sus necesidades de transporte.',
    'group_transportation' => 'Transporte grupal',
    'group_transportation_text' => 'Si se dirige al aeropuerto por negocios o por placer y requiere Transporte Corporativo, Transporte Corporativo Grupal, Servicio de Chofer Ejecutivo, Transporte Anciano, Transporte de Equipo de Carga, Excursiones y Turismo. Reliable Car & Limousine Service dispone de una flota de vehículos nuevos y en buen estado, está listo para trasladarlo desde su hogar, oficina u hotel a cualquiera de los aeropuertos de la zona de Nueva York: JFK, LGA, EWR, TEB, HPN, MCA',
    'technology' => 'Tecnología',
    'technology_text' => 'Reliable Car & Limousine Service utiliza la última tecnología para garantizar que nuestros chóferes estén siempre al tanto de los retrasos climáticos, las condiciones inusuales de la carretera / congestión y otros eventos perturbadores. Nuestros miembros del equipo de limosinas del aeropuerto son expertos en logística y entienden los patrones de viaje en cada uno de los aeropuertos de la ciudad de Nueva York y la región tri-estatal. Su transporte desde o hacia el aeropuerto tendrá una calificación fija, sin aumento de precios u otros costos inesperados.'
];