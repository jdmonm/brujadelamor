<?php

return [
    'about_us' => 'NOSOTROS',
    'services' => 'SERVICIOS',
    'portfolio' => 'PORTAFOLIO',
    'baby_travel' => 'VIAJE BEBE',
    'clients' => 'CLIENTES',
    'contact' => 'CONTACTO'
];