<?php

return [
    'contact_with_us' => 'CONTÁCTANOS',
    'contact_with_us_text' => 'Recibimos con agrado sus comentarios y peticiones, pidenos lo que tu quieras, y nada te será negado.',
    'contact_sub1' => 'Reliable Car & Limousine Service, le ofrece servicio de chófer puerta a puerta, servicio de limusina del aeropuerto en el área New York Tri-State.',
    'contact_sub2' => 'Valoramos a nuestros clientes y creemos que usted merece el mejor servicio, nuestra atención plena e indivisa. Su satisfacción es nuestra prioridad.',
    'contact_sub3' => '(Nuestro servicio de atención al cliente está disponible las 24 horas, los 7 días de la semana, para preguntas, comentarios, inquietudes. ) ',
    'name' => 'Tu Nombre',
    'email' => 'Tu correo',
    'message' => 'Mensaje',
    'send' => 'Enviar'
];