<?php

return [
    'clients_say_about_us' => 'Nuestros clientes dicen de nosotros',
    'clients_say_about_us_text' => 'La satisfacción de nuestros clientes es uno de nuestros principales objetivos, por eso valoramos mucho la opinión y los testimonios finales cuando ya se ha realizado el trabajo. Aquí tiene algunos de estos testimonios de nuestros clientes.'
];