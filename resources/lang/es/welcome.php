<?php

return [
    'slide1' => 'BRUJA DEL AMOR, LO IMPOSIBLE DEJA DE SERLO',
    'slide2' => 'RESULTADOS GARANTIZADOS',
    'slide3' => 'BRUJERIA NATIVA DEL AMAZONAS',

    'book_now' => 'RESERVAR AHORA',
    'call_now' => 'LLAMAR AHORA',

    'about_us' => 'ACERCA DE NOSOTROS',
    'about_us_text' => 'Pertenecemos a una cultura donde los rituales y la magia han sido transmitidos por cientos de generaciones, nuestros ancestros han llevado a cabo rituales con el fin de controlar lo que no ha sido posible para los seres ordinarios, desde la buena fortuna y la suerte, hasta el control sentimental y emociones de las personas aun siguen siendo forzados por nuestra cultura. ',

    'our_reputation_is_now_our_name' => 'NUESTRA REPUTACION ES AHORA NUESTRO NOMBRE',

    'about' => 'nosotros',
    'about_text' => 'Ofrecemos al viajero corporativo de negocios un servicio puerta a puerta, que ofrece transporte directo entre dos ubicaciones geográficas, y es más eficiente para viajes desde y hacia los aeropuertos, servicios de chóferes dirigidos de ciudad a ciudad, diseñados para satisfacer todas sus necesidades de transporte.',

    'community' => 'Comunidad',
    'community_text' => 'Estamos conectados con personas de diferentes partes del mundo gracias a nuestros vehículos elegantes y lujosos y la fiabilidad de nuestro servicio.',

    'most_used_services' => 'SERVICIOS MAS UTILIZADOS', 
    'we_sponsor' => 'Patrocinamos el Hospital St. Jude’s',
    'get_in_touch' => 'Estar en contácto',

];