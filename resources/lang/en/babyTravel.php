<?php

return [
    'baby_travel' => 'BABY TRAVEL',
    'baby_travel_text' => 'Rent baby travel gear for less stress and fewer baggage fees while traveling with young children. Baby equipment rentals make traveling easier and more comfortable for families.',

    'booster_car_seat' => 'BOOSTER CAR SEAT',
    'stroller' => 'STROLLERS',
    'infant_car_seat' => 'INFANT CAR SEAT',
    'safety_first_car_seat' => 'SAFETY FIRST CAR SEAT'
];