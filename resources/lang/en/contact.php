<?php

return [
    'contact_with_us' => 'CONTACT WITH US',
    'contact_with_us_text' => 'We do welcome your comments about our site, as they help us in identifying new areas of interest for future content.',
    'contact_sub1' => 'Reliable Car & Limousine Service, offers you door to door chauffeur, airport limousine service in the New York Tri-State area.',
    'contact_sub2' => 'We value our clients and believe that you deserve the best service, our full and undivided attention. Your satisfaction is our priority.',
    'contact_sub3' => '( Our customer service is available 24/7 for questions, comments, concerns. ) ',
    'name' => 'Your Name',
    'email' => 'Your Email',
    'Message' => 'Message',
    'send' => 'Send'
];