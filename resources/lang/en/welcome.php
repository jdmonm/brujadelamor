<?php

return [
    'slide1' => 'YOUR TRIP IS OUR HIGHEST PRIORITY, OUR BIGGEST COMPLIMENT.',
    'slide2' => 'OUR REPUTATION IS NOW OUR NAME',
    'slide3' => 'YOUR TRIP IS OUR HIGHEST PRIORITY, OUR BIGGEST COMPLIMENT.',

    'book_now' => 'BOOK NOW',
    'call_now' => 'CALL NOW',

    'about_us' => 'ABOUT US',
    'about_us_text' => 'Reliable Car & Limousine Service is a recognized leader in New York limousine service for business travelers, executive passengers and meeting planners in NYC, the tri-state area and beyond. From fast airport transfers to roadshows, and transportation for meetings and conventions, Reliable Car & Limousine Service has, for more than a decade, dedicated our resouces and experience on building a quality customer service-focused exemplary limo service.',

    'our_reputation_is_now_our_name' => 'OUR REPUTATION IS NOW OUR NAME',

    'about' => 'about',
    'community' => 'Community',

    'about_text' => 'We offer the Corporate Business traveler a door-to-door service, featuring direct transportation between two geographic locations, and are most efficient for trips to and from airports, as directed, city to city, chauffeur services designed to meet all your transportation needs.',
    'community_text' => 'We are connected with people from different parts of the world thanks to our elegant and luxurious vehicles and the reliability of our service.',

    'most_used_vehicles' => 'MOST USED VEHICLES', 
    'we_sponsor' => 'We Sponsor St. Jude’s Hospital',
    'get_in_touch' => 'Get in Touch',


];