<?php

return [
    'services' => 'SERVICES',
    'services_text' => 'We have the best service and human team,we are a company with a great track record and experience in the market in this type of services, guaranteeing the satisfaction of our costumers and making their experience something unforgettable', 

    'corporate_bussines' => 'Corporate Business',
    'corporate_bussines_text' => 'We offer the Corporate Business traveler a door-to-door service, featuring direct transportation between two geographic locations, and are most efficient for trips to and from airports, as directed, city to city, chauffeur services designed to meet all your transportation needs.',
    'group_transportation' => 'Group Transportation',
    'group_transportation_text' => 'Whether you are headed to the airport for business or pleasure, Corporate Transportation, Corporate Group Transportation, Executive Chauffeured Service,Elderly Transportation, Cargo Equipment Transportation, Tours and Sightseeing. Reliable Car & Limousine Service fleet of new, well-maintained vehicles stands ready to transfer you from your home, office or hotel to any of the NYC area airports: JFK, LGA, EWR, TEB, HPN, MCA',
    'technology' => 'Technology',
    'technology_text' => 'Reliable Car & Limousine Service uses the latest technology to ensure that our chauffeurs are always aware of weather delays, unusual road conditions/congestion and other disruptive events. Our airport Limo Services team members are logistical experts and understand the travel patterns at each of the NYC airports and the tri-state region. Your transportation to/from the airport will be flat-rated – no surge pricing or other unexpected costs.'
];