<?php

return [
    'about_us' => 'ABOUT US',
    'services' => 'SERVICES',
    'portfolio' => 'PORTFOLIO',
    'baby_travel' => 'BABY TRAVEL',
    'clients' => 'CLIENTS',
    'contact' => 'CONTACT'

];