<?php

return [
    'clients_say_about_us' => 'CLIENTS SAY ABOUT US',
    'clients_say_about_us_text' => 'The satisfaction of our clients is one of our main objectives, for that reason we value the opinion and final testimonies a lot when the work has already been done. Here you have some of these testimonials from our clients.'
];