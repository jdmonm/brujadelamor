<?php

return [
    
	'services' => 'SERVIZI',
    'Services_text' => 'Abbiamo il miglior servizio e la squadra umana, siamo un\'azienda con un track record e grande esperienza nel mercato in questo tipo di servizi, garantendo la soddisfazione dei nostri clienti e rendere la loro esperienza di qualcosa di indimenticabile',

    'corporate_bussines' => 'Impresa aziendale',
    'Corporate_bussines_text' => 'Offriamo il viaggiatore d\'affari Corporate un servizio porta a porta, con il trasporto diretto tra due aree geografiche, e sono più efficienti per i viaggi da e per gli aeroporti, come indicato, di città in città, servizi di autista progettato per soddisfare tutte le vostre esigenze di trasporto. ',
    'group_transportation' => 'Trasporto di gruppo',
    'Group_transportation_text' => 'Se si sono diretti in aeroporto per lavoro o per piacere, Trasporti aziendale, Corporate Group Trasporti, Executive Service autista, trasporto anziani, Trasporto mezzi di trasporto, e escursioni guidate. Reliable Car & Limousine Service di nuovi veicoli, ben curati è pronta per il trasferimento da casa, in ufficio o in albergo ad uno qualsiasi degli aeroporti di New York JFK:, LGA, EWR, TEB, HPN, MCA ',
    'technology' => 'Tecnologia',
    'Technology_text' => 'Reliable Car & Limousine Service utilizzano la più recente tecnologia per garantire che i nostri autisti sono sempre consapevoli dei ritardi meteo, le condizioni della strada insolite / congestione e altri eventi distruttivi. Airport Limousine Servizi I nostri membri del team sono esperti e logistico capire i modelli di viaggi ciascuno degli aeroporti di New York e la regione tri-state. Il tuo trasporto da / per l\'aeroporto sarà valutato in base al prezzo - nessun prezzo o altri costi imprevisti si presenteranno.',
];