<?php

return [
	'baby_travel' => 'viaggio bambino',
    'baby_travel_text' => 'Affitta attrezzatura da viaggio per bambini per ridurre lo stress e meno costi per i bagagli mentre viaggi con bambini piccoli. I noleggi di attrezzature per bambini rendono i viaggi più facili e confortevoli per le famiglie. ',

    'booster_car_seat' => 'SEGGIOLINO AUTO',
    'stroller' => 'PASSEGGERI',
    'infant_car_seat' => 'SEDILE AUTO INFANTILE',
    'safety_first_car_seat' => 'SICUREZZA PRIMO SEGGIOLINO AUTO'
];