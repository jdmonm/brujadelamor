<?php

return [
    'slide1' => 'IL TUO VIAGGIO È LA NOSTRA PRIORITÀ PIÙ ALTA, IL NOSTRO PIÙ GRANDE COMPLIMENTI.',
    'slide2' => 'LA NOSTRA REPUTAZIONE È ORA IL NOSTRO NOME',
    'slide3' => 'IL TUO VIAGGIO È LA NOSTRA PRIORITÀ PIÙ ALTA, IL NOSTRO PIÙ GRANDE COMPLIMENTI.',

    'book_now' => 'PRENOTA ORA',
    'call_now' => 'CHIAMA ORA',

    'about_us' => 'CHI SIAMO',
    'about_us_text' => 'Reliable Car & Limousine Service è un leader riconosciuto nel servizio di limousine di New York per i viaggiatori d\'affari, i passeggeri dei dirigenti e gli organizzatori di riunioni a New York, l\'area dei tre stati e oltre. Dai trasferimenti aeroportuali veloci ai roadshow e ai trasporti per riunioni e convegni, il servizio Reliable Car & Limousine Service ha dedicato per oltre un decennio le nostre risorse e l\'esperienza nella costruzione di un servizio di limousine esemplare incentrato sul servizio clienti. ',

    'our_reputation_is_now_our_name' => 'LA NOSTRA REPUTAZIONE È ORA IL NOSTRO NOME',

    'about' => 'about',
    'community' => 'Community',

    'about_text' => 'Offriamo a chi viaggia per affari aziendale un servizio porta a porta, con trasporto diretto tra due località geografiche, e sono più efficienti per viaggi da e per aeroporti, come servizi diretti, da città a città, con autista progettato per soddisfare tutte le vostre esigenze di trasporto. ',
    'community_text' => 'Siamo connessi con persone provenienti da diverse parti del mondo grazie ai nostri veicoli eleganti e lussuosi e l\'affidabilità del nostro servizio.',

    'most_used_vehicles' => 'VEICOLI PIÙ UTILIZZATI',
    'we_sponsor' => 'Sponsoriamo l\'ospedale di St. Jude',

];