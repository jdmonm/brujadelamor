<?php

return [
    'about_us' => 'NOI',
    'services' => 'SERVIZI',
    'portfloio' => 'PORTAFOGLIO',
    'baby_travel' => 'VIAGGIO PER BAMBINI',
    'clients' => 'CLIENTI',
    'contact' => 'CONTATTO'
];
