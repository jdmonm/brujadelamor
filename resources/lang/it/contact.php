<?php

return [
    'contact_with_us' => 'CONTATTA CON NOI',
    'contact_with_us_text' => 'Apprezziamo i tuoi commenti sul nostro sito, in quanto ci aiutano a identificare nuove aree di interesse per i contenuti futuri.',
    'contact_sub1' => 'Reliable Car & Limousine Service, offre autista porta a porta, servizio di limousine aeroportuale nell\'area di New York Tri-State.',
    'contact_sub2' => 'Apprezziamo i nostri clienti e crediamo che tu meriti il ​​miglior servizio, la nostra piena e indivisa attenzione. La vostra soddisfazione è la nostra priorità. ',
    'contact_sub3' => '(Il nostro servizio clienti è disponibile 24/7 per domande, commenti, dubbi.)',
    'name' => 'il tuo nome',
    'email' => 'La tua email',
    'Message' => 'Messaggio',
    'send' => 'Invia'
];