<?php

return [
    'clients_say_about_us' => 'I CLIENTI DICONO DI NOI',
    'clients_say_about_us_text' => 'La soddisfazione dei nostri clienti è uno dei nostri obiettivi principali, per questo motivo valutiamo molto l\'opinione e le testimonianze finali quando il lavoro è già stato fatto. Ecco alcune di queste testimonianze dei nostri clienti.'
];