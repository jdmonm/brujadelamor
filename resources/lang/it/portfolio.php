<?php

return [
    'portfolio' => 'PORTAFOGLIO',
    'portfolio_TEXT' => 'Scopri alcuni dei nostri veicoli più incredibili e lussuosi che abbiamo a tua disposizione, questo è il nostro vero portafoglio, scegli il tuo preferito e prenota.'
];