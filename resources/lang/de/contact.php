<?php

return [
    
	'contact_with_us' => 'KONTAKT MIT UNS',
    'contact_with_us_text' => 'Wir freuen uns über Ihre Kommentare zu unserer Website, da sie uns dabei helfen, neue Interessengebiete für zukünftige Inhalte zu identifizieren.',
    'contact_sub1' => 'Reliable Car & Limousine Service, bietet Ihnen von Tür zu Tür Chauffeur, Flughafen-Limousinen-Service in der New York Tri-State-Region.',
    'contact_sub2' => 'Wir schätzen unsere Kunden und glauben, dass Sie den besten Service, unsere volle und ungeteilte Aufmerksamkeit verdienen. Ihre Zufriedenheit ist unsere Priorität. ',
    'contact_sub3' => '(Unser Kundendienst steht rund um die Uhr für Fragen, Kommentare und Anliegen zur Verfügung.)',
    'name' => 'Dein Name',
    "E-Mail" => "Ihre E-Mail",
    'Message' => 'Nachricht',
    'send' => 'Senden'
];