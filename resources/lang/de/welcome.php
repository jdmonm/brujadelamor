<?php

return [
    'slide1' => 'IHRE REISE IST UNSERE HÖCHSTE PRIORITÄT, UNSER GRÖSSTES KOMPLIMENT.',
    'slide2' => 'UNSER RUF IST UNSER NAME',
    'slide3' => 'IHRE REISE IST UNSERE HÖCHSTE PRIORITÄT, UNSER GRÖSSTES KOMPLIMENT.',

    'book_now' => 'JETZT BUCHEN',
    'call_now' => 'JETZT ANRUFEN',

    'about_us' => 'ÜBER UNS',
    'about_us_text' => 'Reliable Car & Limousine Service ist ein anerkannter Führer im New Yorker Limousinenservice für Geschäftsreisende, Geschäftsreisende und Tagungsplaner in New York City, dem Dreiländereck und darüber hinaus. Von schnellen Flughafentransfers bis hin zu Roadshows und Transportmöglichkeiten für Meetings und Kongresse - Reliable Car & Limousine Service widmet sich seit mehr als einem Jahrzehnt unseren Ressourcen und unserer Erfahrung beim Aufbau eines auf den Kundenservice fokussierten beispielhaften Limousinenservices. ',

    'our_reputation_is_now_our_name' => 'UNSER RUF IST UNSER NAME',

    'about' => 'ungefähr',
    "community" => "Gemeinschaft",

    'about_text' => 'Wir bieten dem Geschäftsreisenden einen Tür-zu-Tür-Service, der direkten Transport zwischen zwei geografischen Standorten bietet und am effizientesten für Fahrten von und zu Flughäfen ist, wie von Stadt zu Stadt, Chauffeur-Services alle Transportbedürfnisse erfüllen. ',
    'community_text' => 'Dank unserer eleganten und luxuriösen Fahrzeuge und der Zuverlässigkeit unseres Services sind wir mit Menschen aus verschiedenen Teilen der Welt verbunden.',

    "most_used_vehicles" => "MEIST VERWENDETE FAHRZEUGE",
    'we_sponsor' => 'Wir sponsern St. Jude\'s Hospital',
];