<?php

return [
    'services' => 'DIENSTLEISTUNGEN',
    'services_text' => 'Wir haben den besten Service und das menschliche Team, wir sind ein Unternehmen mit einer großen Erfolgsbilanz und Erfahrung auf dem Markt für diese Art von Dienstleistungen, die die Zufriedenheit unserer Kunden garantieren und ihre Erfahrung unvergesslich machen',

    'corporate_bussines' => 'Grossunternehmen',
    'corporate_bussines_text' => 'Wir bieten dem Geschäftsreisenden einen Tür-zu-Tür-Service mit direktem Transport zwischen zwei geografischen Standorten an und sind am effizientesten für Fahrten von und zu Flughäfen, wie von Stadt zu Stadt, Chauffeurdienste, gerichtet alle Transportbedürfnisse erfüllen. ',
    'group_transportation' => 'Gruppentransport',
    'group_transportation_text' => 'Egal, ob Sie geschäftlich oder privat zum Flughafen fahren, Corporate Transportation, Unternehmensgruppe Transportation, Executive Chauffeured Service, Älterer Transport, Cargo Equipment Transportation, Touren und Sightseeing. Reliable Car & Limousine Service Service-Flotte von neuen, gut gewarteten Fahrzeugen steht bereit, um Sie von Ihrem Haus, Büro oder Hotel zu einem der Flughäfen in New York City zu bringen: JFK, LGA, EWR, TEB, HPN, MCA ',
    "technology" => "Technologie",
    'technology_text' => 'Reliable Car & Limousine Service Service nutzt die neueste Technologie, um sicherzustellen, dass unsere Chauffeure immer auf Wetterverspätungen, ungewöhnliche Straßenverhältnisse / Staus und andere störende Ereignisse achten. Unsere Mitarbeiter am Flughafen Limo Services sind Logistikexperten und verstehen die Reisemuster an jedem der Flughäfen in New York und in der Dreistaatenregion. Ihr Transport zum / vom Flughafen wird pauschal sein - keine Preisstaffelung oder andere unerwartete Kosten.',
];