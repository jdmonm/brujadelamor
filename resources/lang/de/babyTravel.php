<?php

return [
	'baby_travel' => 'BABYREISE',
    'baby_travel_text' => 'Mieten Sie Reiseausrüstung für weniger Stress und weniger Gepäckgebühren, wenn Sie mit kleinen Kindern reisen. Babyverleih macht das Reisen für Familien einfacher und bequemer.',

    'booster_car_seat' => 'KINDERAUTOSITZ',
    "stroller" => "KINDERWAGEN",
    'infant_car_seat' => 'BABYSCHALE',
    'Safety_first_car_seat' => 'SICHERHEIT ERSTER AUTO-SITZ'
];