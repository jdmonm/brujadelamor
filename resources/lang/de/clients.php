<?php

return [
    
	'clients_say_about_us' => 'KUNDEN SAGEN ÜBER UNS',
    'clients_say_about_us_text' => 'Die Zufriedenheit unserer Kunden ist eines unserer Hauptziele, deshalb schätzen wir die Meinung und das Abschlusszeugnis sehr, wenn die Arbeit bereits erledigt ist. Hier haben Sie einige dieser Referenzen von unseren Kunden. '
];