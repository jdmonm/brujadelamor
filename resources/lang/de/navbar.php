<?php

return [
    'about_us' => 'WIR',
    'services' => 'DIENSTLEISTUNGEN',
    'portfolio' => 'PORTFOLIO',
    'baby_travel' => 'BABY-REISE',
    'clients' => 'KUNDEN',
    'contact' => 'KONTAKT'
];