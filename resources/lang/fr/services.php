<?php

return [
    'services' => 'SERVICES',
    'services_text' => 'Nous avons le meilleur service et une équipe humaine, nous sommes une entreprise avec une grande expérience dans le marché de ce type de services, garantissant la satisfaction de nos clients et rendant leur expérience inoubliable',

    'corporate_bussines' => 'Corporate Business',
    'corporate_bussines_text' => 'Nous offrons aux voyageurs d\'affaires un service porte-à-porte, avec un transport direct entre deux emplacements géographiques, et qui est le plus efficace pour les trajets vers et depuis les aéroports. répondre à tous vos besoins de transport.',
    'group_transportation' => 'Transport de groupe',
    'group_transportation_text' => 'Que vous vous rendiez à l\'aéroport pour affaires ou pour le plaisir, transport d\'entreprise, transport de groupe corporatif, service de chauffeur de direction, transport pour personnes âgées, transport d\'équipement de fret, excursions et visites touristiques. Reliable Car & Limousine Service flotte de nouveaux véhicules bien entretenus prêts à vous transférer de votre domicile, bureau ou hôtel à l\'un des aéroports de la région de New York: JFK, LGA, EWR, TEB, HPN, MCA ',
    'technologie' => 'Technologie',
    'technology_text' => 'Reliable Car & Limousine Service utilise la dernière technologie pour s\'assurer que nos chauffeurs sont toujours au courant des retards de temps, des conditions de route inhabituelles / congestion et d\'autres événements perturbateurs. Nos membres de l\'équipe Limo Services de l\'aéroport sont des experts en logistique et comprennent les habitudes de voyage de chacun des aéroports de New York et de la région des trois États. Votre transport vers / de l\'aéroport sera forfaitaire - pas de surtension ni d\'autres coûts inattendus.',
];