<?php

return [
    
    'slide1' => 'VOTRE VOYAGE EST NOTRE PLUS HAUTE PRIORITÉ, NOTRE PLUS GRAND COMPLIMENT.',
    'slide2' => 'NOTRE RÉPUTATION EST MAINTENANT NOTRE NOM',
    'slide3' => 'VOTRE VOYAGE EST NOTRE PLUS HAUTE PRIORITÉ, NOTRE PLUS GRAND COMPLIMENT.',

    'book_now' => 'RESERVER MAINTENANT',
    'call_now' => 'CALL NOW',

    'about_us' => 'A PROPOS DE NOUS',
    'about_us_text' => 'Reliable Car & Limousine Service est un leader reconnu dans le service de limousine de New York pour les voyageurs d\'affaires, les passagers exécutifs et les planificateurs de réunions à New York, dans la région des trois États et au-delà. Des transferts aéroportuaires rapides aux roadshows, en passant par le transport pour les réunions et les conventions, Reliable Car & Limousine Service consacre depuis plus d\'une décennie ses ressources et son expérience à la construction d\'un service de limousine exemplaire et orienté vers le service client.',

    'our_reputation_is_now_our_name' => 'NOTRE RÉPUTATION EST MAINTENANT NOTRE NOM',

    'about' => 'environ',
    'communauté' => 'Communauté',

    'about_text' => 'Nous offrons aux voyageurs d\'affaires un service porte-à-porte, avec un transport direct entre deux emplacements géographiques, et sont plus efficaces pour les trajets vers et depuis les aéroports, comme indiqué, ville à répondre à tous vos besoins de transport.',
    'community_text' => 'Nous sommes connectés avec des gens de différentes parties du monde grâce à nos véhicules élégants et luxueux et à la fiabilité de notre service.',

    'most_used_vehicles' => 'VÉHICULES LES PLUS UTILISÉS',
    'we_sponsor' => 'Nous sponsorisons l\'hôpital St. Jude\'s',

];