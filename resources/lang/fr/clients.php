<?php

return [
    'clients_say_about_us' => 'LES CLIENTS DISENT SUR NOUS',
    'clients_say_about_us_text' => 'La satisfaction de nos clients est l\'un de nos principaux objectifs, c\'est pour cette raison que nous apprécions beaucoup l\'opinion et les témoignages finaux lorsque le travail a déjà été fait. Voici quelques-uns de ces témoignages de nos clients.'
];