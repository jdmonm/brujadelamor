<?php

return [
    
	'contact_with_us' => 'CONTACTEZ-NOUS',
    'contact_with_us_text' => 'Vos commentaires sur notre site sont les bienvenus car ils nous aident à identifier de nouveaux domaines d\'intérêt pour le contenu futur.',
    'contact_sub1' => 'Reliable Car & Limousine Service, vous offre un service de chauffeur de porte à porte, un service de limousine d\'aéroport dans la région de Tri-State à New York.',
    'contact_sub2' => 'Nous apprécions nos clients et croyons que vous méritez le meilleur service, notre attention pleine et entière. Votre satisfaction est notre priorité.',
    'contact_sub3' => '(Notre service client est disponible 24h / 24 et 7j / 7 pour les questions, commentaires, préoccupations.)',
    'name' => 'Votre nom',
    'email' => 'Votre Email',
    'Message' => 'Message',
    'send' => 'Envoyer'
];