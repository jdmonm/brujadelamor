<?php

return [
    
	'baby_travel' => 'BABY TRAVEL',
    'baby_travel_text' => 'Louez votre équipement de voyage pour moins de stress et moins de frais de bagages lorsque vous voyagez avec de jeunes enfants. La location d\'équipement pour bébé rend le voyage plus facile et plus confortable pour les familles.',

    'booster_car_seat' => 'BOOSTER CAR SEAT',
    'poussette' => 'POUSSETTES',
    'infant_car_seat' => 'SIEGE AUTO POUR BÉBÉ',
    'safety_first_car_seat' => 'SÉCURITÉ FIRST CAR SEAT'
];