@extends('layout.app')


@section('content')

<section class="section-34 section-bottom-78">
    <div class="shell">
      <h2 class="divider offset-top-40 offset-md-top-80 text-center">Portafolio de servicios</h2>
      <div class="range offset-md-top-108">
        <div class="cell-xs-6 cell-md-3">
          <div class="thumbnail text-center thumbnail-product"><a href="single.html"><img src=" {{ asset('images/portfolio/amarres.jpg') }} " alt="" width="270" height="160"/></a>
            <div class="caption">
              <div class="h5"><a href="single.html" class="txt-matrix">AMARRES DE AMOR</a></div>
              <div class="h4 reveal-inline-block"></div> <span class="txt-darker reveal-block reveal-lg-inline-block inset-lg-left-5">
                <del></del></span><a href=" {{ route('amarres-de-amor-descripcion') }} " class="btn btn-sm btn-primary btn-min-width-210-lg">Ver mas ...</a>
            </div>
          </div>
        </div>
        <div class="cell-xs-6 cell-md-3">
          <div class="thumbnail text-center thumbnail-product"><a href="single.html"><img src=" {{ asset('images/portfolio/hechizosdeamor.jpg') }} " alt="" width="270" height="160"/></a>
            <div class="caption">
              <div class="h5"><a href="single.html" class="txt-matrix">HECHIZOS DE AMOR</a></div>
              <div class="h4 reveal-inline-block"></div> <span class="txt-darker reveal-block reveal-lg-inline-block inset-lg-left-5">
                <del></del></span><a href=" {{ route('hechizos-de-amor-descripcion') }} " class="btn btn-sm btn-primary btn-min-width-210-lg">Ver mas ...</a>
            </div>
          </div>
        </div>
        <div class="cell-xs-6 cell-md-3 offset-top-10 offset-md-top-0">
          <div class="thumbnail text-center thumbnail-product"><a href="single.html"><img src=" {{ asset('images/portfolio/amarresdelmismosexo.jpg') }} " alt="" width="270" height="160"/></a>
            <div class="caption">
              <div class="h5"><a href="single.html" class="txt-matrix">AMARRES DEL MISMO SEXO</a></div>
              <div class="h4 reveal-inline-block"></div> <span class="txt-darker reveal-block reveal-lg-inline-block inset-lg-left-5">
                <del></del></span><a href=" {{ route('amarres-del-mismo-sexo-descripcion') }} " class="btn btn-sm btn-primary btn-min-width-210-lg">Ver mas ...</a>
            </div>
          </div>
        </div>
        <div class="cell-xs-6 cell-md-3 offset-top-10 offset-md-top-0">
          <div class="thumbnail text-center thumbnail-product"><a href="single.html"><img src=" {{ asset('images/portfolio/endulzamientos.jpg') }} " alt="" width="270" height="160"/></a>
            <div class="caption">
              <div class="h5"><a href="single.html" class="txt-matrix">ENDULZAMIENTOS</a></div>
              <div class="h4 reveal-inline-block"></div> <span class="txt-darker reveal-block reveal-lg-inline-block inset-lg-left-5">
                <del></del></span><a href=" {{ route('endulzamientos-descripcion') }} " class="btn btn-sm btn-primary btn-min-width-210-lg">Ver mas ...</a>
            </div>
          </div>
        </div>
        <div class="cell-xs-6 cell-md-3 offset-top-10">
          <div class="thumbnail text-center thumbnail-product"><a href="single.html"><img src=" {{ asset('images/portfolio/suerteydinero.jpg') }} " alt="" width="270" height="160"/></a>
            <div class="caption">
              <div class="h5"><a href="single.html" class="txt-matrix">SUERTE Y DINERO</a></div>
              <div class="h4 reveal-inline-block"></div> <span class="txt-darker reveal-block reveal-lg-inline-block inset-lg-left-5">
                <del></del></span><a href="#" class="btn btn-sm btn-primary btn-min-width-210-lg">Ver mas ...</a>
            </div>
          </div>
        </div>
        <div class="cell-xs-6 cell-md-3 offset-top-10">
          <div class="thumbnail text-center thumbnail-product"><a href="single.html"><img src=" {{ asset('images/portfolio/chakra.jpg') }} " alt="" width="270" height="160"/></a>
            <div class="caption">
              <div class="h5"><a href="single.html" class="txt-matrix">LIMPIAS Y CHAKRAS</a></div>
              <div class="h4 reveal-inline-block"></div> <span class="txt-darker reveal-block reveal-lg-inline-block inset-lg-left-5">
                <del></del></span><a href="#" class="btn btn-sm btn-primary btn-min-width-210-lg">Ver mas ...</a>
            </div>
          </div>
        </div>
        <div class="cell-xs-6 cell-md-3 offset-top-10">
          <div class="thumbnail text-center thumbnail-product"><a href="single.html"><img src=" {{ asset('images/portfolio/vudu.jpg') }} " alt="" width="270" height="160"/></a>
            <div class="caption">
              <div class="h5"><a href="single.html" class="txt-matrix">VUDÚ</a></div>
              <div class="h4 reveal-inline-block"></div> <span class="txt-darker reveal-block reveal-lg-inline-block inset-lg-left-5">
                <del></del></span><a href="#" class="btn btn-sm btn-primary btn-min-width-210-lg">Ver mas ...</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

@endsection