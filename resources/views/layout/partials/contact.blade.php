<section class="section-34 section-md-bottom-83 section-bottom-45" id="contact">
  <div class="shell">
    <h2 class="divider offset-top-80 text-center">@lang('contact.contact_with_us')</h2>
    <div class="range">
      <div class="cell-md-8">
        <h3 class="txt-black text-center">@lang('welcome.get_in_touch')</h3>
        <p class="offset-top-27">@lang('contact.contact_with_us_text')</p>
        <!-- RD Mailform-->



        <form data-form-output="form-output-global" data-form-type="contact" method="post" action="send-mail.php" class="rd-mailform text-left offset-top-86">
          <div class="range">
            <div class="cell-sm-6">
              <div class="form-group">
                <label for="contact-name-1" class="form-label">@lang('contact.name')</label>
                <input id="contact-name-1" type="text" name="name" data-constraints="@Required" class="form-control"/>
              </div>
            </div>
            <div class="cell-sm-6 offset-top-48">
              <div class="form-group">
                <label for="contact-email-1" class="form-label">@lang('contact.email')</label>
                <input id="contact-email-1" type="email" name="email" data-constraints="@Required @Email" class="form-control"/>
              </div>
            </div>
            <div class="cell-sm-6">
              <div class="form-group">
                <label for="contact-name-1" class="form-label">Asunto</label>
                <input id="contact-name-1" type="text" name="subject" data-constraints="@Required" class="form-control"/>
              </div>
            </div>
            <div class="cell-sm-6 offset-top-48">
              <div class="form-group">
                <label for="contact-email-1" class="form-label">Teléfono</label>
                <input id="contact-email-1" type="email" name="phone" data-constraints="@Required" class="form-control"/>
              </div>
            </div>
            <div class="cell-xs-12 offset-top-48">
              <div class="form-group">
                <label for="contact-message" class="form-label">@lang('contact.message')</label>
                <textarea id="contact-message" name="message" data-constraints="@Required" class="form-control"></textarea>
              </div>
            </div>
            <div class="cell-xs-12 text-center offset-top-27">
              <button type="submit" class="btn btn-primary btn-sm btn-min-width-230">@lang('contact.send')</button>
            </div>
          </div>
        </form>
      </div>

      <div class="cell-md-4">
        <div class="inset-md-left-25">
          <div class="range">
            <div class="cell-xs-6 cell-sm-3 cell-md-12 cell-xs-push-1">
              <h5 class="txt-matrix">Siguenos</h5>
              <ul class="list-inline list-inline-variant-4 offset-top-7">
                <li><a href="#" class="icon icon-xxs icon-darker fa-facebook"></a></li>
                <li><a href="#" class="icon icon-xxs icon-darker fa-twitter"></a></li>
                <li><a href="#" class="icon icon-xxs icon-darker fa-pinterest"></a></li>
                <li><a href="#" class="icon icon-xxs icon-darker fa-vimeo"></a></li>
                <li><a href="#" class="icon icon-xxs icon-darker fa-google"></a></li>
              </ul>
            </div>
            <div class="cell-xs-6 cell-sm-3 cell-md-12 offset-top-36 offset-sm-top-0 offset-md-top-36 cell-xs-push-3 cell-md-push-2">
              <h5 class="txt-matrix">Teléfono</h5>
              <ul class="offset-top-10">
                <li><a href="callto:#2137856772" class="txt-black">213 785 6772</a></li>
              </ul>
            </div>
            <div class="cell-xs-6 cell-sm-3 cell-md-12 offset-top-36 offset-xs-top-0 offset-md-top-36 cell-xs-push-2 cell-md-push-3">
              <h5 class="txt-matrix">E-mail</h5><a href="mailto:#labrujadelamor1@gmail.com" class="txt-matrix offset-top-10 reveal-inline-block">labrujadelamor1@gmail.com</a>
            </div>
            <div class="cell-xs-6 cell-sm-3 cell-md-12 offset-top-36 offset-sm-top-0 offset-md-top-36 cell-xs-push-4">
              <h5 class="txt-matrix">Dirección</h5>
              <address class="address offset-top-7">
                <p>Los Angeles California</p>
              </address>
            </div>
            <div class="cell-xs-6 cell-sm-4 cell-md-12 cell-xs-push-5">
              <h5 class="txt-matrix offset-top-36">Horario de atención</h5>
              <p class="offset-top-7">Mon–Fri 9:00am–7:00pm</p>
              <p class="offset-top-0">Sat–Sun 10:00am–5:00pm</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

