<footer class="page-foot bg-ebony section-top-78 section-bottom-15">
  <div class="shell text-center text-sm-left">
    <div class="range">
      <div class="cell-sm-6 cell-md-3">
        <div class="rd-navbar-brand">
          <a href="index.html" class="brand-name">
            <div class="brand-logo">
              <img src="images/logo.png" alt="" width="235" height="71"/>
            </div>
          </a>
        </div>
      </div>
      <div class="cell-sm-6 cell-md-3 cell-md-preffix-1 text-left">
        <div class="contact-info reveal-block">
          <span class="icon icon-xs icon-white icon-border fa-phone"></span>
          <span>
            <a href="callto:#">
              <span>212 256 1770<br>Llamanos</span>
            </a>
          </span>
        </div>
        <address class="address reveal-block offset-top-20 txt-darker">
          <span class="icon icon-xs icon-white icon-border fa-map-marker"></span>
          <span>
            <a href="contacts.html">52 10th Ave</a>
          </span>
        </address>
        <div class="time reveal-block offset-top-20 txt-darker">
          <span class="icon icon-xs icon-white icon-border fa-clock-o"></span>
          <span>Mon–Fri 9:00–19:00<br>Sat–Sun 10:00–17:00</span>
        </div>
        </div>
        <div class="cell-sm-12 cell-md-5 offset-top-30 offset-sm-top-60 offset-md-top-0">
          <p></p>
        </div>
        <div class="cell-xs-12 offset-top-40 offset-lg-top-100">
          <div class="hr-double"></div>
          <p class="small txt-darker">&#169; <span id="copyright-year"></span> All Rights Reserved Terms of Use and 
            <a href="privacy.html" class="txt-primary">Privacy Policy
            </a>
          </p>
      </div>
    </div>
  </div>
</footer>