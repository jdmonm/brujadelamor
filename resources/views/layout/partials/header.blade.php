<header class="page-head">
  <!-- RD Navbar-->
  <div class="rd-navbar-wrap">
    <nav class="rd-navbar" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fullwidth" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-sm-stick-up-offset="50px" data-lg-stick-up-offset="200px" data-stick-up="true" data-sm-stick-up="true" data-md-stick-up="true" data-lg-stick-up="true"><span data-rd-navbar-toggle="#rd-navbar-right-panel" data-custom-toggle-disable-on-blur="true" class="rd-navbar-right-panel-toggle"></span>
      <div class="rd-navbar-inner">
        <div class="rd-navbar-panel-wrap">
          <!-- RD Navbar Panel-->
          <div class="rd-navbar-panel">
            <!-- RD Navbar Toggle-->
            <button data-rd-navbar-toggle=".rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
            <!-- RD Navbar Brand-->
            <div class="rd-navbar-brand brand-static">
              <a href="index.html" class="brand-name">
                <div class="brand-logo">
                  <h3 style="color:red">BRUJA DEL AMOR</h3>
                </div>
              </a>
            </div>
          </div>
          <div id="rd-navbar-right-panel" class="rd-navbar-right-panel">
            <address class="address">
              <span class="icon icon-sm icon-white icon-border fa-map-marker"></span>
              <span class="txt-darker">
                <a href="#contact">Los Angeles California</a>
              </span>
            </address>
            <div class="time">
              <span class="icon icon-sm icon-white icon-border fa-clock-o"></span>
              <span>Mon–Fri 9:00–19:00 <br>Sat–Sun 10:00–17:00</span>
            </div>
            <div class="contact-info">
              <span class="icon icon-sm icon-white icon-border fa-phone"></span>
              <span>
                <a href="callto:#2137856772">
                  <span>213 785 6772 <br>Llamanos</span> <br />
                </a>
              </span>
            </div>
          </div>
        </div>
        <div class="rd-navbar-nav-wrap">
          <!-- RD Navbar Brand-->
          <div class="rd-navbar-brand brand-fixed">
            <a href="index.html" class="brand-name">
              <div class="brand-logo">
                <!-- <img src="images/logo.png" alt="" width="235" height="71"/> -->
              </div>
            </a>
          </div>
          <!-- RD Navbar Nav-->
          <ul class="rd-navbar-nav">
            <li><a href=" {{ url('/') }} ">NOSOTROS</a></li>
            <li><a href=" {{ url('/servicios') }} ">SERVICIOS</a></li>
            <li><a href=" {{ url('/portafolio') }} ">PORTAFOLIO</a></li>
            <li><a href=" {{ url('/testimonios') }} ">TESTIMONIOS</a></li>
            <li><a href="#contact">CONTÁCTO</a></li>
            <!-- RD Navbar Dropdown-->

            </li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
</header>