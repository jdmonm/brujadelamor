@extends('layout.app')


@section('content')

<main class="page-content">
    <!--sections Services-->
    <section class="section-34 section-bottom-78">
      <div class="shell">
        <h2 class="divider offset-top-40 offset-md-top-80 text-center">Hechizos de amor</h2>
        <div class="range offset-md-top-50">
          <div class="cell-xs-12 cell-md-11">
            <!-- Responsive-tabs-->
            <div class="responsive-tabs responsive-tabs-default offset-top-40 responsive-tabs-default-variant-2 text-left">
              <ul class="resp-tabs-list text-right resp-tabs-list-2">
                <li><span>Definicion</span></li>
                <li><span>Efectos</span></li>
              </ul>
              <div class="resp-tabs-container resp-tabs-container-2">
                <!--Definición-->
                <div class="tiempo">
                  <h3 class="txt-black">Definición</h3>
                  <p>Los hechizos de amor son cualquier tipo de conjuro realizado en personas que tienen como fin alterar sentimentalmente al recipiente ritual en ambitos relacionados con el amor.</p><br>
                  <p>Entre los servicios mas utilizados están los rompimientos, curación de una ruptura amorosa, atracción, incluso la lujuría están catalogados dentro de hechizos de amor.  </p>
                  <br></h4><a href="#" class="btn btn-sm btn-primary offset-top-10">Contrata este servicio</a>
                </div>
                <!--Efectos-->
                <div>
                  <h3 class="txt-black">Efectos</h3>
                  <p class="offset-top-20">Algunos de los efectos causados por los hechizos de atracción en el recipiente ritual son la anciedad, melancolia por soledad, deseo carnal, mareos, depresión, esto sucede porque la persona en la que es practicado el hechizo de atracción, se sentira vacía y solamente encontrará paz al estar junto a usted.</p>
                  <br>
                  <p>Mientras que cuando se realizan hechizos personales con el fin de terminar la melancolia por un rompimiento, sentirá como su corazón se endurece y sentirá atracción por otras personas, mejorará su autoestima y no le gustará pasar mucho tiempo en su casa, las ganas de explorar el mundo, salir y conocer otras personas serán muy normales en su cotidianidad</p>
                </div>
                <!--Tiempo
                <div>
                  <h3 class="txt-black">Tiempo</h3>
                  <p class="offset-top-20">El tiempo varia según el hechizo, entre los mas rápidos estan los hechizos con el fin de atraer personas, siguen los hechizos con el fin de curar decepciones amorosas, los hechizos que mas suelen tardar son los que tienen como fin ligar sexualmente con el elegido. </p>
                </div>-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
</main>     

@endsection