@extends('layout.app')


@section('content')

<main class="page-content">
    <!--sections Services-->
    <section class="section-34 section-bottom-78">
      <div class="shell">
        <h2 class="divider offset-top-40 offset-md-top-80 text-center">Amarres del mismo sexo</h2>
        <div class="range offset-md-top-50">
          <div class="cell-xs-12 cell-md-11">
            <!-- Responsive-tabs-->
            <div class="responsive-tabs responsive-tabs-default offset-top-40 responsive-tabs-default-variant-2 text-left">
              <ul class="resp-tabs-list text-right resp-tabs-list-2">
                <li><span>Definicion</span></li>
                <li><span>Efectos</span></li>
                
              </ul>
              <div class="resp-tabs-container resp-tabs-container-2">
                <!--Definición-->
                <div class="tiempo">
                  <h3 class="txt-black">Definición</h3>
                  <p>Este arquetipo de conjuro tiene la misma función que un amarre de amor tradicional, con la diferencia que se enfoca en un recipiente ritual del mismo género que el solicitante, es decir, un amarre de amor gay, es capaz de unir a 2 hombres o a 2 mujeres aunque no tengan tendencias homosexuales, es un hechizo muy poderoso y muy efectivo que obliga a tener atracción por personas del mismo genero </p>
                  <br></h4><a href="#" class="btn btn-sm btn-primary offset-top-10">Contrata este servicio</a>
                </div>
                <!--Efectos-->
                <div>
                  <h3 class="txt-black">Efectos</h3>
                  <p class="offset-top-20">Primordialmente, este conjuro causa un efecto inicial sobre pensamientos tiernos y cariñosos con personas del mismo sexo, aunque la persona sea consiente y quiera cambiar sus pensamientos, no podrá, cada vez serán mas fuertes hasta el punto incluso de llegar a imaginar momentos de lujuria imposibles de reprimir, sentira cambios hormonales que desatan efectos de atracción sobre el mismo genero llegando incluso a sentir mas atracción por personas del mismo sexo que por el sexo contrario.  </p>
                  <br>
                  <p>Una vez que ha concluida la primera etapa, se iniciara uno de nuestros rituales para llevar a cabo la atadura, se impondrá un velo para que solamente pueda ver a través del corazón del solicitante permitiendo sentir los mismos deseos que que el solicitante del conjuro.</p>
                </div>
                <!--Tiempo
                <div>
                  <h3 class="txt-black">Tiempo</h3>
                  <p class="offset-top-20">El tiempo varia según el hechizo, entre los mas rápidos estan los hechizos con el fin de atraer personas, siguen los hechizos con el fin de curar decepciones amorosas, los hechizos que mas suelen tardar son los que tienen como fin ligar sexualmente con el elegido. </p>
                </div>-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
</main>     

@endsection