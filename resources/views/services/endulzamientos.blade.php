@extends('layout.app')


@section('content')

<main class="page-content">
    <!--sections Services-->
    <section class="section-34 section-bottom-78">
      <div class="shell">
        <h2 class="divider offset-top-40 offset-md-top-80 text-center">Amarres de amor</h2>
        <div class="range offset-md-top-50">
          <div class="cell-xs-12 cell-md-11">
            <!-- Responsive-tabs-->
            <div class="responsive-tabs responsive-tabs-default offset-top-40 responsive-tabs-default-variant-2 text-left">
              <ul class="resp-tabs-list text-right resp-tabs-list-2">
                <li><span>Definicion</span></li>
                <li><span>Efectos</span></li>
              </ul>
              <div class="resp-tabs-container resp-tabs-container-2">
                <!--Definición-->
                <div class="tiempo">
                  <h3 class="txt-black">Definición</h3>
                  <p>Un endulzamiento,tiene como finalidad conseguir que una persona que en algún momento sintió algo por ti, vuelva a sentirlo, puede ser igual o incluso mas fuerte que la primera vez, su función principal tiene como resultado el avivamiento de sentimientos escondidos u olvidados por el recipiente ritual.</p><br>
                  <p>Para comprender mejor su efecto, es capaz de potencias los sentimientos de tu ex-pareja o pareja actual para que se sienta un enamoramiento pleno, incluso despues de mucho tiempo.  </p>
                  <br></h4><a href="#" class="btn btn-sm btn-primary offset-top-10">Contrata este servicio</a>
                </div>
                <!--Efectos-->
                <div>
                  <h3 class="txt-black">Efectos</h3>
                  <p class="offset-top-20">Tu pareja empezará a verte muy atractivo, tanto así que no podrá dejar de verte, notará solamente tus cualidades y fortalezas y querra estar a tu lado porque se sentirá segura, tu imagen rondará su cabeza durante todo el tiempo que no este contigo y contará las horas para verte.</p>
                </div>
                <!--Tiempo
                <div>
                  <h3 class="txt-black">Tiempo</h3>
                  <p class="offset-top-20">El tiempo es relativo, para cada quien es diferente y entre mas susceptible sea el recipiente ritual será realizado el trabajo con mayor brevedad y menor tiempo.</p>

                  <p>Generalmente los trabajos son realizados según instrucciones y peticiones con un enfoque gradual según la petición y el amarre deseado por lo que los trabajos son realizados en muy poco tiempo, siempre y cuando estes seguro sobre que es lo que deseas.</p>
                </div>-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
</main>     

@endsection