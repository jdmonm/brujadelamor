@extends('layout.app')


@section('content')
<section id="clients" class="parallax-section">
    <div class="container">
        <div class="clients-wrapper">
            <div class="row text-center">
                <div class="col-sm-8 col-sm-offset-2">
                    <br><br>
                    <h2>@lang('clients.clients_say_about_us')</h2>
                    <p class="divider"> @lang('clients.clients_say_about_us_text')</p>
                </div>
            </div>
            <br><br><br>
            <center>
            <div id="clients-carousel" class="carousel slide" data-ride="carousel"> <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#clients-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#clients-carousel" data-slide-to="1"></li>
                    <li data-target="#clients-carousel" data-slide-to="2"></li>
                </ol> <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="single-client">
                            <div class="media">
                                <div class="media-body">
                                    <blockquote><p>Great Customer service and were on time. Thanks VZ</p><small>Roma Shamalov</small><a href="https://www.facebook.com/pg/1Reliablelimo/reviews/?ref=page_internal">www.facebook.com</a></blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="single-client">
                            <div class="media">
                                <div class="media-body">
                                    <blockquote><p>Great Customer service and were on time. Thanks VZ</p><small>Roma Shamalov</small><a href="https://www.facebook.com/pg/1Reliablelimo/reviews/?ref=page_internal">www.facebook.com</a></blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="single-client">
                            <div class="media">
                                <div class="media-body">
                                    <blockquote><p>Great Customer service and were on time. Thanks VZ</p><small>Roma Shamalov</small><a href="https://www.facebook.com/pg/1Reliablelimo/reviews/?ref=page_internal">www.facebook.com</a></blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </center>
        </div>
    </div>
</section><!--/#clients-->

@endsection