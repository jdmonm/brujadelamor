@extends('layout.app')

@section('content')
<div data-height="" data-min-height="" class="swiper-container swiper-slider">
  	<div class="swiper-wrapper">
	    <div data-slide-bg="images/slide1.jpg" class="swiper-slide">
	    	<div class="jumbotron-wrap">
          <div class="jumbotron-variant-1">
            <h1 class="heading animated bounceInRight" style="white-space: normal;">@lang('welcome.slide1')</h1>
            	<br>
            	
              <br><br>
            <div class="btn-group-variant-1">
            </div>
          </div>
	  		</div>
	    </div>
	    <div data-slide-bg="images/slide3.jpg" class="swiper-slide">
	    	<div class="jumbotron-wrap">
          <div class="jumbotron-variant-1">
            	<h1 style="white-space: normal;"> @lang('welcome.slide2') </h1>
            	<br>
              <br><br>
            	<div class="btn-group-variant-1"></div>
          	</div>
	  			</div>
	    	</div>
    	<div data-slide-bg="images/slide2.jpg" class="swiper-slide">
    		<div class="jumbotron-wrap">
            	<div class="jumbotron-variant-1">
              		<h1 class="heading animated bounceInRight" style="white-space: normal;">@lang('welcome.slide3')</h1>
              		<br>
	            	  <br><br>
              		<div class="btn-group-variant-1">
              		</div>
	            </div>
  			</div>
    	</div>
  	</div>
  <!-- Swiper Navigation-->
  <div class="swiper-button-prev"></div>
  <div class="swiper-button-next"></div>
</div>

<section class="section-88 bg-pampas">
  	<div class="shell">
	    <h2 class="divider text-center">@lang('welcome.about_us')</h2>
    	<div class="range">
      		<div class="cell-md-8 cell-md-preffix-2">
        		<p style="text-align: center;">@lang('welcome.about_us_text')</p>
      		</div>
    	</div>
  	</div>
</section>

<!--sections Modern Skill Bars-->
<section class="section-88 section-bottom-45">
  	<div class="shell">
	    <h2 class="divider text-center">@lang('welcome.most_used_services')</h2>
    	<div class="range">
      		<div class="cell-sm-6 cell-sm-preffix-3">
        		<h5 class="txt-base">AMARRES DE AMOR</h5>
        		<div class="progress"><span>55</span>
          			<div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" data-stroke="12" style="width:55%" class="progress-bar progress-bar-comet progress-bar-striped progress-bar-horizontal"></div>
        		</div>
        		<h5 class="txt-base offset-top-50">AMARRES DEL MISMO SEXO</h5>
        		<!-- Progress Bar-->
        		<div class="progress"><span>15</span>
          			<div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" data-stroke="12" style="width:15%" class="progress-bar progress-bar-comet progress-bar-striped progress-bar-horizontal"></div>
    			  </div>
        		<h5 class="txt-base offset-top-50">ENDULZAMIENTOS</h5>
        		<!-- Progress Bar-->
        		<div class="progress"><span>20</span>
          			<div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" data-stroke="12" style="width:20%" class="progress-bar progress-bar-comet progress-bar-striped progress-bar-horizontal">
          			</div>
        		</div>
        		<h5 class="txt-base offset-top-50">SUERTE Y DINERO </h5>
        		<!-- Progress Bar-->
        		<div class="progress"><span>14</span>
          			<div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" data-stroke="12" style="width:14%" class="progress-bar progress-bar-comet progress-bar-striped progress-bar-horizontal"></div>
        		</div>
        		<h5 class="txt-base offset-top-50">LIMPIAS Y CHAKRAS</h5>
        		<!-- Progress Bar-->
        		<div class="progress"><span>6</span>
          			<div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" data-stroke="12" style="width:6%" class="progress-bar progress-bar-comet progress-bar-striped progress-bar-horizontal"></div>
        		</div>
      		</div>
    	</div>
  	</div>
</section>

<section>
  <div class="col-md-4 col-sm-4"></div>
    <div class="col-md-4 col-sm-4 col-xs-12">
      <center>
        <br><br>
        <h4>Hemos ayudado a cientos de personas a obtener todo lo que su corazón desea sin efectos secundarios, miles de trabajos realizados al pie de la letra, obteniendo el resultado esperado, no debes conformarte con la realidad, debes controlar tu vida, tu felicidad y tu tranquilidad vale mas que cualquier cosa.</h4>
      </center>
    </div>
    <div class="col-md-4 col-sm-4"></div>
</section>

<section class="section-88 section-bottom-45"></section>
<section class="section-88 section-bottom-45"></section>
<section class="section-88 section-bottom-45"></section>
<section class="section-88 section-bottom-45"></section>
@endsection