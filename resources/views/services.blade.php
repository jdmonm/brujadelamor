@extends('layout.app')


@section('content')

<!-- RD Parallax-->
<section class="rd-parallax">
	<div data-speed="0.5" data-type="media" data-url=" {{ asset('images/services/services.jpg') }} " class="rd-parallax-layer"></div>
	<div data-speed="0" data-type="html" class="rd-parallax-layer">
		<div class="shell text-center txt-matrix section-60 section-md-83">
			<h2 class="divider" style="color: white">SERVICIOS DE AMOR</h2>
			<div class="range">
				<div class="cell-md-10 cell-md-preffix-1">
					<h5 style="color: white">Nuestros servicios del amor son capaces de doblegar cualquier corazón, los sentimientos son controlados por nuestra magía según la voluntad de la persona que nos contácta, el espiritismo, el mistisismo y la santería juegan de nuestro lado como mejor aliado  y nos permite realizar cualquier tipo de solicitud; incluso forzamos corazones y mentes para que sientan y piensen lo que deseamos, lo anterior indiferente a clases sociales, generos </h5>
				</div>
			</div>
			<br>
			<h2 class="divider" style="color: white">LIMPIAS Y CHAKRAS</h2>
			<div class="range">
				<div class="cell-md-10 cell-md-preffix-1">
					<h5 style="color: white">Sientes que las energías negativas invaden tu propio espacio, efectos de hechicería sobre ti, mala suerte, definitivamente necesitas una limpieza o una purificación de chakras, nosotros contamos con las habilidades necesarias para llevar a cabo este proceso y devolverte tu tranquilidad, no debes temer cuando cuentas con el hechizo adecuado.</h5>
				</div>
			</div>
			<br>
			<h2 class="divider"</h2>
			<div class="range">
				<div class="cell-md-10 cell-md-preffix-1">
					<h5></h5>
				</div>
			</div>
		</div>
	</div>
</section>


@endsection