<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::get('/servicios', function(){
	return view('services');
});

Route::get('/portafolio', function(){
	return view('portfolio');
});

Route::get('/portafolio/amarres-de-amor', function(){
	return view('services.amarres-de-amor');
})->name('amarres-de-amor-descripcion');

Route::get('/portafolio/hechizos-de-amor', function(){
	return view('services.hechizos-de-amor');
})->name('hechizos-de-amor-descripcion');

Route::get('/portafolio/amarres-del-mismo-sexo', function(){
	return view('services.amarres-del-mismo-sexo');
})->name('amarres-del-mismo-sexo-descripcion');

Route::get('/portafolio/endulzamientos', function(){
	return view('services.endulzamientos');
})->name('endulzamientos-descripcion');

Route::get('/testimonios', function(){
	return view('testimonies');
});